package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserLockByIdCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "lock user by id";

    @NotNull
    public static final String DESCRIPTION = "lock user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        getAdminEndpoint().lockUserById(getSession(), id);
    }

}
