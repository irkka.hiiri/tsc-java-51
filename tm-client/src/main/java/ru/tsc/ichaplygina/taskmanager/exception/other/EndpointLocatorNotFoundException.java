package ru.tsc.ichaplygina.taskmanager.exception.other;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class EndpointLocatorNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Endpoint locator not found!";

    public EndpointLocatorNotFoundException() {
        super(MESSAGE);
    }

}
