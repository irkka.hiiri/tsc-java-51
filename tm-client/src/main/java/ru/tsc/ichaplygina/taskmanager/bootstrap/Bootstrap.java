package ru.tsc.ichaplygina.taskmanager.bootstrap;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.ichaplygina.taskmanager.api.EndpointLocator;
import ru.tsc.ichaplygina.taskmanager.api.repository.ICommandRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.ICommandService;
import ru.tsc.ichaplygina.taskmanager.api.service.ILogService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.comparator.CommandComparator;
import ru.tsc.ichaplygina.taskmanager.component.FileScanner;
import ru.tsc.ichaplygina.taskmanager.endpoint.*;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandDescriptionEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.CommandNameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.CommandNotFoundException;
import ru.tsc.ichaplygina.taskmanager.repository.CommandRepository;
import ru.tsc.ichaplygina.taskmanager.service.CommandService;
import ru.tsc.ichaplygina.taskmanager.service.LogService;
import ru.tsc.ichaplygina.taskmanager.service.PropertyService;
import ru.tsc.ichaplygina.taskmanager.util.SystemUtil;
import ru.tsc.ichaplygina.taskmanager.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@Getter
@Setter
public final class Bootstrap implements EndpointLocator {

    private static final boolean CONSOLE_LOG_ENABLED = true;

    @NotNull
    private static final String PID_FILE_NAME = "task-manager.pid";

    @NotNull
    private final ILogService logService = new LogService(CONSOLE_LOG_ENABLED);

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();

    @NotNull
    private final ProjectEndpoint projectEndpoint = projectEndpointService.getProjectEndpointPort();

    @NotNull
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();

    @NotNull
    private final TaskEndpoint taskEndpoint = taskEndpointService.getTaskEndpointPort();

    @NotNull
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();

    @NotNull
    private final SessionEndpoint sessionEndpoint = sessionEndpointService.getSessionEndpointPort();

    @NotNull
    private final AdminEndpointService adminEndpointService = new AdminEndpointService();

    @NotNull
    private final AdminEndpoint adminEndpoint = adminEndpointService.getAdminEndpointPort();

    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService, commandService);

    @Nullable
    public Session session;

    public void executeCommand(@NotNull final String commandName) {
        if (isEmptyString(commandName)) return;
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getCommands().get(commandName))
                .orElseThrow(() -> new CommandNotFoundException(commandName));
        execute(command);
    }

    public void executeCommandByArgument(@NotNull final String argument) {
        @NotNull final AbstractCommand command = Optional.ofNullable(commandService.getArguments().get(argument))
                .orElseThrow(() -> new CommandNotFoundException(argument));
        execute(command);
    }

    public void execute(@NotNull final AbstractCommand command) {
        command.execute();
    }

    @SneakyThrows
    public void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.tsc.ichaplygina.taskmanager.command");
        @NotNull final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.ichaplygina.taskmanager.command.AbstractCommand.class)
                .stream()
                .sorted(CommandComparator.getInstance())
                .collect(Collectors.toList());
        boolean isAbstract;
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registerCommand(clazz.newInstance());
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(PID_FILE_NAME), pid.getBytes());
        @NotNull final File file = new File(PID_FILE_NAME);
        file.deleteOnExit();
    }

    @SuppressWarnings("InfiniteLoopStatement")
    public void processInput() {
        logService.info("Program run in command-line mode.");
        showWelcome();
        @NotNull String command = readCommand();
        while (true) {
            try {
                if (isEmptyString(command)) {
                    command = readCommand();
                    continue;
                }
                logService.command("Executing command: " + command);
                executeCommand(command);
                System.out.println(APP_COMMAND_SUCCESS);
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println(APP_COMMAND_ERROR);
            }
            command = readCommand();
        }
    }

    @NotNull
    private String readCommand() {
        return TerminalUtil.readLine(COMMAND_PROMPT);
    }

    public void registerCommand(@NotNull final AbstractCommand command) {
        try {
            @NotNull final String terminalCommand = command.getCommand();
            @NotNull final String commandDescription = command.getDescription();
            @Nullable final String commandArgument = command.getArgument();
            if (isEmptyString(terminalCommand)) throw new CommandNameEmptyException();
            if (isEmptyString(commandDescription)) throw new CommandDescriptionEmptyException();
            command.setEndpointLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (isEmptyString(commandArgument)) return;
            commandService.getArguments().put(commandArgument, command);
        } catch (@NotNull AbstractException e) {
            logService.error(e);
        }
    }

    public void run(@NotNull final String... args) {
        initPID();
        initCommands();
        fileScanner.init();
        if (args.length == 0) processInput();
        else
            try {
                logService.info("Program run in arguments mode.");
                executeCommandByArgument(args[0]);
            } catch (@NotNull final AbstractException e) {
                logService.error(e);
            }
    }

    private void showWelcome() {
        printLinesWithEmptyLine(APP_WELCOME_TEXT);
    }

}
