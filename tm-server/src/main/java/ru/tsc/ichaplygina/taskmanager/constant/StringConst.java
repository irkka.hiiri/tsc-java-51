package ru.tsc.ichaplygina.taskmanager.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class StringConst {

    @NotNull
    public static final String APP_COMMAND_ERROR = "An error has occurred.";
    @NotNull
    public static final String APP_COMMAND_SUCCESS = "Command completed successfully.";
    @NotNull
    public static final String COMMAND_PROMPT = "> ";
    @NotNull
    public static final String DELIMITER = " : ";
    @NotNull
    public static final String EMPTY = "";
    @NotNull
    public static final String NEW_LINE = "\n";
    @NotNull
    public static final String APP_WELCOME_TEXT = "*** WELCOME TO THE ULTIMATE TASK MANAGER ***" +
            NEW_LINE +
            NEW_LINE +
            "Enter <help> to show available commands." +
            NEW_LINE +
            "Enter <exit> to quit." +
            NEW_LINE +
            "Enter command: ";
    @NotNull
    public static final String APP_HELP_HINT_TEXT = "Run with command-line arguments (as listed in [brackets])." +
            NEW_LINE +
            "Run with no arguments to enter command-line mode." +
            NEW_LINE +
            NEW_LINE +
            "Available commands and arguments:";
    @NotNull
    public static final String PLACEHOLDER = "<empty>";
    @NotNull
    public static final String SORT_INPUT = "Sort output by: (optional) ";
    @NotNull
    public static final String SYSINFO_FREE_MEMORY = "Free memory: ";
    @NotNull
    public static final String SYSINFO_MAX_MEMORY = "Maximum memory: ";
    @NotNull
    public static final String SYSINFO_NO_LIMIT_TEXT = "No limit";
    @NotNull
    public static final String SYSINFO_PROCESSORS = "Available processors: ";
    @NotNull
    public static final String SYSINFO_TOTAL_MEMORY = "Total memory available to JVM: ";

    @NotNull
    public static final String SYSINFO_USED_MEMORY = "Memory used by JVM: ";

}
