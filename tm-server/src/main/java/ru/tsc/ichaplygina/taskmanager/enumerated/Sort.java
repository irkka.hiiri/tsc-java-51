package ru.tsc.ichaplygina.taskmanager.enumerated;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.comparator.*;

import java.util.Comparator;

public enum Sort {

    CREATED("CREATED - Sort by created date", ComparatorByCreated.getInstance()),
    STARTED("STARTED - Sort by start date", ComparatorByDateStart.getInstance()),
    FINISHED("FINISHED - Sort by finish date", ComparatorByDateFinish.getInstance()),
    NAME("NAME - Sort by name", ComparatorByName.getInstance()),
    STATUS("STATUS - Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final Comparator comparator;
    @NotNull
    private final String displayName;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
