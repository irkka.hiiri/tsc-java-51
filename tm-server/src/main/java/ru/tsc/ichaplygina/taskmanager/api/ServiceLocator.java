package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.model.IUserService;

public interface ServiceLocator {

    @NotNull IConnectionService getConnectionService();

    @NotNull IPropertyService getPropertyService();

    @NotNull IUserService getUserService();

}
