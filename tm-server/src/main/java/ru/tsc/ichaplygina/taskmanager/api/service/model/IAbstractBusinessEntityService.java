package ru.tsc.ichaplygina.taskmanager.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.AbstractBusinessEntity;

import java.util.List;

public interface IAbstractBusinessEntityService<E extends AbstractBusinessEntity> extends IAbstractService<E> {

    void add(@NotNull String userId, @NotNull String entityName, @Nullable String entityDescription);

    void clear(String userId);

    @Nullable E completeById(@NotNull String userId,
                             @NotNull String projectId);

    E completeByIndex(@NotNull String userId, int index);

    @Nullable E completeByName(@NotNull String userId, @NotNull String projectName);

    @NotNull List<E> findAll(@NotNull String userId);

    @NotNull List<E> findAll(@NotNull String userId, @NotNull String sortBy);

    @Nullable E findById(@NotNull String userId, @Nullable String entityId);

    E findByIndex(@NotNull String userId, int index);

    @Nullable E findByName(@NotNull String userId, @NotNull String entityName);

    @Nullable String getId(@NotNull String userId, int index);

    @Nullable String getId(@NotNull String userId, @NotNull String entityName);

    long getSize(@NotNull String userId);

    boolean isEmpty(@NotNull String userId);

    @Nullable E removeById(@NotNull String userId, @NotNull String projectId);

    E removeByIndex(@NotNull String userId, int index);

    @Nullable E removeByName(@NotNull String userId, @NotNull String entityName);

    @Nullable E startById(@NotNull String userId, @NotNull String projectId);

    E startByIndex(@NotNull String userId, int index);

    @Nullable E startByName(@NotNull String userId, @NotNull String projectName);

    @Nullable E updateById(@NotNull String userId,
                           @NotNull String entityId,
                           @NotNull String entityName,
                           @Nullable String entityDescription);

    E updateByIndex(@NotNull String userId,
                    int index,
                    @NotNull String entityName,
                    @Nullable String entityDescription);
}
