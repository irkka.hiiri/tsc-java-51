package ru.tsc.ichaplygina.taskmanager.exception.security;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public final class UserSelfDeleteNotAllowedException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Cannot remove logged-on user.";

    public UserSelfDeleteNotAllowedException() {
        super(MESSAGE);
    }

}
