package ru.tsc.ichaplygina.taskmanager.component;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.ISenderService;
import ru.tsc.ichaplygina.taskmanager.dto.EntityLogDTO;
import ru.tsc.ichaplygina.taskmanager.service.SenderService;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final ISenderService service = new SenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull final String type) {
        es.submit(() -> {
            @NotNull final EntityLogDTO entity = service.createMessage(object, type);
            service.send(entity);
        });
    }

    public void stop() {
        es.shutdown();
    }


}
